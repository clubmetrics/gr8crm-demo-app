navigation = {
    //'public'(global: true) {
    //}
    main(global: true) {
        crmTask controller: 'crmTask', action: 'index', title: 'crmTask.index.label'
        crmCalendar controller: 'crmCalendar', action: 'index', title: 'crmCalendar.index.label'
	crmInvoice controller: 'crmInvoice', action: 'index', title: 'crmInvoice.index.label'
	crmCampaign controller: 'crmCampaign', action: 'index', title: 'crmCampaign.index.label'
	crmOrder controller: 'crmOrder', action: 'index', title: 'crmOrder.index.label'
	crmProduct controller: 'crmProduct', action:'index', title: 'crmProduct.index.label'
	crmBlog controller: 'crmBlogPost', action:'index', title: 'crmBlog.index.label'
	crmFeature controller: 'crmFeature', action:'index', title: 'crmFeature.index.label'

 }
}
